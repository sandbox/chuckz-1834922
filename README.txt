
-- SUMMARY --

The Encrypted Local Stream Wrapper module extends the temporary and private
stream wrapper classes to provide encryption for files.
This module is a tool intended be used by other Drupal module developers
and does not provide functionality that a non-developer can use directly.
A typical usage scenario would be to encrypt generated PDF files with
sensitive information.

For a full description of the module, visit the project page:
  http://drupal.org/project/encrypt_local

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/encrypt_local

-- REQUIREMENTS --

The PHP mcrypt library must be enabled on your server
http://php.net/manual/en/book.mcrypt.php

-- INSTALLATION --

There are no special installation or administration steps beyond the usual.

-- CONFIGURATION --

There are no direct configuration settings for this module.
Configuration settings are determined by the
temporary and private file locations.

The storage location for encrypted temporary files will be in the
'etemporary' subdirectory of the configured temporary files directory.

The storage location for encrypted private files will be in the
'eprivate' subdirectory of the configured private files directory.

-- CUSTOMIZATION --

There are no customization settings for this module.

-- USAGE SCENARIOS --

The easiest way to understand this module is to understand the
primary usage scenario for which it was developed.

THE OLD WAY
A health-care counselor visits a school and collects student information.
Based on that health information, the web site generates a PDF file report
to be printed and taken home to the child's parents.
This PDF file is generated by some other Drupal module and is saved to the
temporary file directory.
A link to the PDF file (system/temporary/foo.pdf) is included in a results
page and the health-care provider can click on this link to open the PDF
file in Acrobat Reader for printing.

The danger with approach is that the PDF file is stored unencrypted
in the temorary directory. If there is a security breach then this private
student health information could be read.

WITH THIS MODULE
The Drupal module that generates the PDF file saves it to the encrypted
temporary directory. This PDF file gets encrypted and a cookie is created
for the counselor's browser with the encryption key.
The link to the PDF file now looks like this (encrypted/temporary/foo.pdf)
When the counselor clicks on this link, the file gets decrypted
using the key from the cookie, and the file is decrypted before being
sent to the counselor's browser.
Any attempt to read this PDF file without the encryption key will fail.

-- DESIGN OVERVIEW --

CLASSES
Two concrete classes implement this module's functionality:
EncryptedPrivateStreamWrapper
EncryptedTemproaryStreamWrapper

They both extend an abstract class EncryptedLocalStreamWrapper
which has most of the complex encrypting logic.

If PHP supported C++ style polymorphism, I would have had these classes
extend both the EncryptedLocalStreamWrapper and the appropriate
DrupalPrivateStreamWrapper or DrupalTemporaryStreamWrapper.
Instead, the concrete classes create delegates of the appropriate class
and have the delegates do the non-encryption work.

FILE LOCATIONS
Rather than define two new independent file locations for encrypted
private and temporary files, this design simply creates subdirectories
in delegate's root directory (eprivate and etemporary).
This simplifies configuration, and allows this module to leverage
automatic temporary file cleanup.

COOKIES AND ENCRYPTION KEYS
The encryption algorithm used is the rijndael-256 block algorithm.
The key created is a 32-byte (256 bit) key.

A new key is automatically created each time a file is written
and there is currently no way to re-use an existing key for encrypting.

The key is stored in a cookie as a 64-character string which is a
hexadecimal representation of the 256 bit key.
The name of the cookie is the file's Drupal URI with periods
replaced by underscores and prefixed with 'encrypted_local_'.

Drupal URI: etemporary://subdir_x.bar/foo.pdf
Cookie: $_COOKIE['encrypted_local_etemporary://subdir_x_bar/foo_pdf']

The cookie expires in DRUPAL_MAXIMUM_TEMP_FILE_AGE which is 6 hours.
So, for temporary files the lifetime of the cookie and file are the same.
For private files, the module developer needs a mechanism to save the
cookie permanently and re-supply it as needed.

-- SAMPLE CODE --

Refer to the test cases give some simple usage.

Notice that the cookie management is automatically handled,
though you would want to add a way to store and retrieve the
encryption key for files intended to be stored for more than
six hours.

Here is some code that you can use to get the a link:
$external_name = file_create_url('etemporary://subdir_x.bar/foo.pdf');

<a href="{$external_name}">
  <img
    title="application/pdf"
    src="/modules/file/icons/application-pdf.png" />Open PDF Report</a>


-- CONTACT --

Current maintainers:
* Chuck Zalinski (ChuckZ) - http://drupal.org/user/741152

This project has been sponsored by:
* Behavioral Health Applications
  Technical solutions and grant support for Behavioral Health professionals.
  http://www.behavioralhealthapplications.com
